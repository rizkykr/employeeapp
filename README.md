# EmployeeManagement

Project ini dibuat dengan [Angular CLI](https://github.com/angular/angular-cli) versi 17.1.3 dengan database Supabase. aplikasi ini dibuat sesuai dengan ketentuan soal yang diberikan. semoga hasilnya dapat membawa saya dapat tergabung.

## Cara Running

Ketik perintah `ng serve` untuk mode development atau dengan `ng build` yang akan menghasilkan aplikasi pada folder `dist/`. ketika dihalaman login gunakan username `admin@google.com` dan password `12345`.
