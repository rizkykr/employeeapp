import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { NavigationService } from './navigation.service';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  imports: [RouterOutlet, HeaderComponent, RouterLink, RouterLinkActive],
})
export class AppComponent {
  title = 'employee-management';
  constructor(private navigation: NavigationService) {}
}
