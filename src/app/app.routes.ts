import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { EmployeeComponent } from './pages/employee/employee.component';
import { DetailPegawaiComponent } from './pages/employee/detail-pegawai/detail-pegawai.component';
import { AddpegawaiComponent } from './pages/employee/addpegawai/addpegawai.component';
import { ErrorPageComponent } from './pages/404/404.component';
import { LoginComponent } from './pages/auth/login/login.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'employee/detail/:id', component: DetailPegawaiComponent },
  { path: 'employee/addpegawai', component: AddpegawaiComponent },
  { path: '**', component: ErrorPageComponent },
];
