import { NgFor } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, NgFor],
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  linkMenu = [
    { title: 'Beranda', link: '/' },
    { title: 'Data Pegawai', link: '/employee' },
  ];
}
