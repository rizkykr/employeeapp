import { Component } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SupabaseService } from '../../../supabase.service';

@Component({
  selector: 'app-login',
  standalone: true,
  templateUrl: './login.component.html',
  imports: [RouterOutlet, ReactiveFormsModule],
})
export class LoginComponent {
  loading = false;
  loginForm = this.formBuilder.group({
    email: '',
    password: '',
  });
  constructor(
    private readonly supabase: SupabaseService,
    private readonly formBuilder: FormBuilder,
    private router: Router
  ) {}

  async onSubmit(): Promise<void> {
    try {
      this.loading = true;
      const email = this.loginForm.value.email as string;
      const password = this.loginForm.value.password as string;
      console.log(this.loginForm.value);
      const { error, data } = await this.supabase.signIn(email, password);
      console.log(error);
      console.log(data);
      if (error) throw error;
      this.router.navigate(['/']);
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    } finally {
      this.loginForm.reset();
      this.loading = false;
    }
  }
}
