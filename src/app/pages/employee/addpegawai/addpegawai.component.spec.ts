import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpegawaiComponent } from './addpegawai.component';

describe('AddpegawaiComponent', () => {
  let component: AddpegawaiComponent;
  let fixture: ComponentFixture<AddpegawaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AddpegawaiComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AddpegawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
