import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { SupabaseService } from '../../../supabase.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addpegawai',
  standalone: true,
  imports: [RouterLink, ReactiveFormsModule],
  templateUrl: './addpegawai.component.html',
})
export class AddpegawaiComponent {
  loading = false;
  today = new Date();
  dd = String(this.today.getDate()).padStart(2, '0');
  mm = String(this.today.getMonth() + 1).padStart(2, '0');
  yyyy = this.today.getFullYear();
  todayDate = this.yyyy + '-' + this.mm + '-' + this.dd;

  addPegawaiForm = this.formBuilder.group({
    username: new FormControl('', Validators.required),
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    basicSalary: '',
    status: '',
    group: '',
    desc: '',
    lastEducation: '',
    gender: '',
  });

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly supabase: SupabaseService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  notifikasi(
    type: 'success' | 'error' | 'warning' | 'info',
    text: string,
    header?: string
  ) {
    this.toastr[type](text, header || 'Action Button');
  }

  async onSubmit(): Promise<void> {
    try {
      this.loading = true;
      const dtform = this.addPegawaiForm.value;
      const arrData = {
        username: dtform.username as string,
        firstName: dtform.firstName as string,
        lastName: dtform.lastName as string,
        email: dtform.email as string,
        birthDate: dtform.birthDate as string,
        basicSalary: dtform.basicSalary as string,
        status: dtform.status as string,
        group: dtform.group as string,
        desc: dtform.desc as string,
        lastEducation: dtform.lastEducation as string,
        gender: dtform.gender as string,
      };
      const { error, data } = await this.supabase.saveDataPegawai(arrData);
      console.log(error);
      console.log(data);
      if (error) {
        this.notifikasi('error', 'Gagal menambahkan pegawai', 'Gagal');
        throw error;
      } else {
        this.notifikasi('success', 'Berhasil menambahkan pegawai', 'Sukses');
        this.router.navigate(['/employee']);
      }
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    } finally {
      this.addPegawaiForm.reset();
      this.loading = false;
    }
  }
}
