import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SupabaseService } from '../../../supabase.service';
import { convert_to_rupiah } from '../../../../function/tools';
import { NavigationService } from '../../../navigation.service';

@Component({
  selector: 'app-detail-pegawai',
  standalone: true,
  imports: [],
  templateUrl: './detail-pegawai.component.html',
})
export class DetailPegawaiComponent {
  idPeg = '';
  dataPeg: any = null;
  gajiPeg = '';
  constructor(
    private route: ActivatedRoute,
    private readonly supabase: SupabaseService,
    private navigation: NavigationService
  ) {}
  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id')! as string;
    this.idPeg = id;
    this.loadData();
  }
  back(): void {
    this.navigation.back();
  }
  async loadData() {
    try {
      const {
        data: dataTable,
        error,
        status,
      } = await this.supabase.loadDataPegawaiSingle({ id: this.idPeg });
      if (error && status !== 406) {
        throw error;
      }
      if (dataTable) {
        this.dataPeg = dataTable;
        this.gajiPeg = convert_to_rupiah(dataTable.basicSalary);
      }
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    }
  }
}
