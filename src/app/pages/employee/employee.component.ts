import { Component } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { SupabaseService } from '../../supabase.service';
import { NgFor, NgIf } from '@angular/common';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { convert_to_rupiah } from '../../../function/tools';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee',
  standalone: true,
  imports: [RouterLink, NgFor, NgIf, ReactiveFormsModule],
  templateUrl: './employee.component.html',
})
export class EmployeeComponent {
  dataTabel: any[] = [];
  searchValue = '';
  sizeData = 10;
  curPage: number = 1;
  sortArr: any = {};
  sortString: any[] = [];
  constructor(
    private route: ActivatedRoute,
    private readonly supabase: SupabaseService,
    private readonly formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService
  ) {}

  async ngOnInit(): Promise<void> {
    this.route.queryParamMap.subscribe((params: any) => {
      const prm: any = params.params;
      if (prm.search) {
        this.searchValue = prm.search;
      }
      if (prm.size) {
        this.sizeData = prm.size;
      }
      if (prm.page) {
        this.curPage = parseInt(prm.page);
      }
      if (prm.sort) {
        this.sortArr = JSON.parse(prm.sort);
        this.sortString = Object.entries(this.sortArr);
      }
      if (prm) this.loadData();
    });
    this.loadData();
  }
  notifikasi(
    type: 'success' | 'error' | 'warning' | 'info',
    text: string,
    header?: string
  ) {
    this.toastr[type](text, header || 'Action Button');
  }
  async deletePegawai(id: string) {
    if (confirm('Yakin ingin menghapus pegawai ini?') == true) {
      try {
        const { error, status } = await this.supabase.deleteDataPegawaiSingle({
          id: id,
        });
        if (error && status !== 406) {
          this.notifikasi('error', 'Gagal menghapus data pegawai', 'Gagal');
          throw error;
        } else {
          this.notifikasi(
            'success',
            'Berhasil menghapus data pegawai',
            'Sukses'
          );
          this.loadData();
        }
      } catch (error) {
        if (error instanceof Error) {
          alert(error.message);
        }
      }
    }
  }
  triggerSearch(val: any) {
    const vl = val ? { search: val.target.value } : {};
    this.router.navigate(['/employee'], {
      queryParams: vl,
      queryParamsHandling: 'merge',
    });
  }
  clearSort() {
    this.router.navigate(['/employee']);
    this.sortArr = {};
    this.sortString = [];
    this.searchValue = '';
    this.loadData();
  }
  formatRupiah(val: number) {
    return convert_to_rupiah(val);
  }
  sortData(field: string) {
    const prvCheck = this.sortArr[field];
    this.sortArr[field] = !prvCheck;
    this.router.navigate(['/employee'], {
      queryParams: {
        sort: JSON.stringify(this.sortArr),
      },
      queryParamsHandling: 'merge',
    });
  }
  onAdjustSize(value: any) {
    this.router.navigate(['/employee'], {
      queryParams: {
        size: value.target.value,
      },
      queryParamsHandling: 'merge',
    });
  }
  prevPage() {
    if (this.curPage > 1) {
      this.router.navigate(['/employee'], {
        queryParams: {
          page: this.curPage - 1,
        },
        queryParamsHandling: 'merge',
      });
    }
  }
  nextPage() {
    this.router.navigate(['/employee'], {
      queryParams: {
        page: this.curPage + 1,
      },
      queryParamsHandling: 'merge',
    });
  }
  async loadData() {
    try {
      const {
        data: dataTable,
        error,
        status,
      } = await this.supabase.loadDataPegawai({
        limit: this.sizeData,
        page: this.curPage,
        search: this.searchValue,
        sort: this.sortArr,
      });
      if (error && status !== 406) {
        throw error;
      }

      if (dataTable) {
        this.dataTabel = dataTable;
      }
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    }
  }
}
