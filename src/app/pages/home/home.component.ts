import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
// import { SupabaseService } from '../../supabase.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './home.component.html',
})
export class HomeComponent {
  // statistik = {
  //   totalPegawai: 0,
  //   pegawaiTetap: 0,
  //   pegawaiKontrak: 0,
  //   pegS1: 0,
  // };
  // constructor(private readonly supabase: SupabaseService) {}
  // async ngOnInit(): Promise<void> {
  //   this.loadData();
  // }
  // async loadData() {
  //   try {
  //     const { data: total } = await this.supabase.loadStatistik();
  //     if (total) this.statistik.totalPegawai = total?.length;
  //     const { data: tetap } = await this.supabase.loadStatistik(
  //       'status',
  //       'Tetap'
  //     );
  //     if (tetap) this.statistik.pegawaiTetap = tetap?.length;
  //     const { data: kontrak } = await this.supabase.loadStatistik(
  //       'status',
  //       'Kontrak'
  //     );
  //     if (kontrak) this.statistik.pegawaiKontrak = kontrak?.length;
  //     const { data: sarjana } = await this.supabase.loadStatistik(
  //       'lastEducation',
  //       'S1'
  //     );
  //     if (sarjana) this.statistik.pegS1 = sarjana?.length;
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }
}
