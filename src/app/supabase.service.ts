import { Injectable } from '@angular/core';
import {
  AuthChangeEvent,
  createClient,
  Session,
  SupabaseClient,
  User,
} from '@supabase/supabase-js';

import { environment } from '../environment';

export interface IUser {
  email: string;
  name: string;
  website: string;
  url: string;
}

@Injectable({
  providedIn: 'root',
})
export class SupabaseService {
  private supabaseClient: SupabaseClient;

  constructor() {
    this.supabaseClient = createClient(
      environment.supabaseUrl,
      environment.supabaseKey
    );
  }

  public getUser() {
    return this.supabaseClient.auth.getUser();
  }

  public getSession() {
    return this.supabaseClient.auth.getSession();
  }

  public authChanges(
    callback: (event: AuthChangeEvent, session: Session | null) => void
  ): any {
    return this.supabaseClient.auth.onAuthStateChange(callback);
  }

  public signIn(email: string, pass: string) {
    return this.supabaseClient.auth.signInWithPassword({
      email: email,
      password: pass,
    });
  }

  public signOut(): Promise<any> {
    return this.supabaseClient.auth.signOut();
  }

  loadDataPegawai({
    limit = 10,
    page = 1,
    search,
    sort,
  }: {
    limit?: number;
    page?: number;
    search?: string;
    sort?: any;
  }) {
    const mustNowPage = limit * page;
    var dbCon = this.supabaseClient
      .from('employee')
      .select()
      .range(mustNowPage - limit, mustNowPage - 1);
    if (search) {
      dbCon = dbCon.or(
        'firstName.ilike.%' +
          search +
          '%,lastName.ilike.%' +
          search +
          '%,email.ilike.%' +
          search +
          '%,group.ilike.%' +
          search +
          '%,description.ilike.%' +
          search +
          '%'
      );
    }
    if (Object.keys(sort).length) {
      Object.keys(sort).forEach((key) => {
        const value = sort[key];
        dbCon = dbCon.order(key, { ascending: value });
      });
    }
    return dbCon;
  }

  loadDataPegawaiSingle({ id }: { id: string }) {
    return this.supabaseClient.from('employee').select().eq('id', id).single();
  }
  deleteDataPegawaiSingle({ id }: { id: string }) {
    return this.supabaseClient.from('employee').delete().match({ id: id });
  }

  loadStatistik(eqfield?: string, eqval?: string) {
    var sql = this.supabaseClient
      .from('employee')
      .select('*', { count: 'exact' });
    if (eqfield && eqval) {
      sql = sql.eq(eqfield, eqval);
    }
    return sql;
  }

  saveDataPegawai({
    username,
    firstName,
    lastName,
    email,
    birthDate,
    basicSalary,
    status,
    group,
    desc,
    lastEducation,
    gender,
  }: {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    birthDate: string;
    basicSalary: string;
    status: string;
    group: string;
    desc: string;
    lastEducation: string;
    gender: string;
  }) {
    const arrData = {
      username: username,
      firstName: firstName,
      lastName: lastName,
      email: email,
      birthDate: birthDate,
      basicSalary: basicSalary,
      status: status,
      group: group,
      description: desc,
      gender: gender,
      lastEducation: lastEducation,
    };
    return this.supabaseClient.from('employee').insert([arrData]).select();
  }
}
